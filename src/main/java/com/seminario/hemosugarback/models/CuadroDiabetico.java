package com.seminario.hemosugarback.models;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 *
 * @author David Silva
 */
@Entity
@Table(name = "cuadrodiabetico")
public class CuadroDiabetico {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 200)
    private String descripcion;

    @NotBlank
    private Double rangoinicial;

    @NotBlank
    private Double rangofinal;
    
    public CuadroDiabetico() {
    }

    public CuadroDiabetico(String descripcion, Double rangoinicial, Double rangofinal) {
        this.descripcion = descripcion;
        this.rangoinicial = rangoinicial;
        this.rangofinal = rangofinal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getRangoinicial() {
        return rangoinicial;
    }

    public void setRangoinicial(Double rangoinicial) {
        this.rangoinicial = rangoinicial;
    }

    public Double getRangofinal() {
        return rangofinal;
    }

    public void setRangofinal(Double rangofinal) {
        this.rangofinal = rangofinal;
    }
    
}
