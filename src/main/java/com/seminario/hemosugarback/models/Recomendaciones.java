package com.seminario.hemosugarback.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 *
 * @author David Silva
 */
@Entity
@Table(name = "recomendaciones")
public class Recomendaciones {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 2000)
    private String descripcion;

    @NotBlank
    @ManyToOne
    @JoinColumn(name = "cuadroDiabetico")
    private CuadroDiabetico cuadroDiabetico;

    public Recomendaciones() {
    }

    public Recomendaciones(String descripcion, CuadroDiabetico cuadroDiabetico) {
        this.descripcion = descripcion;
        this.cuadroDiabetico = cuadroDiabetico;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public CuadroDiabetico getCuadroDiabetico() {
        return cuadroDiabetico;
    }

    public void setCuadroDiabetico(CuadroDiabetico cuadroDiabetico) {
        this.cuadroDiabetico = cuadroDiabetico;
    }

}
