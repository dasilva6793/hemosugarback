/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.hemosugarback.repository;

import com.seminario.hemosugarback.models.CuadroDiabetico;
import com.seminario.hemosugarback.models.Recomendaciones;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author David Silva
 */
@Repository
public interface RecomendacionesRepository extends JpaRepository<Recomendaciones, Long> {

    public List<Recomendaciones> findAllById(Long cuadro);

    @Query("select r from #{#entityName} r where r.cuadroDiabetico = ?1")
    public List<Recomendaciones> getRecomendacionesByCudre(CuadroDiabetico c);

}
