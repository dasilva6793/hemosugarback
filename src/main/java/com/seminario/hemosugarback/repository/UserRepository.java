package com.seminario.hemosugarback.repository;

import com.seminario.hemosugarback.models.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author David Silva
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);
    
    Boolean existsByCedula(String cedula);

    //Boolean existsByEmail(String email);
}
