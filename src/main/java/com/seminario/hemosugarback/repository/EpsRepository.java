package com.seminario.hemosugarback.repository;

import com.seminario.hemosugarback.models.Eps;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author David Silva
 */
@Repository
public interface EpsRepository extends JpaRepository<Eps, Long> {

   
}
