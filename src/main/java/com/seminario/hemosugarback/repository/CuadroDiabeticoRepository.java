/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.hemosugarback.repository;

import com.seminario.hemosugarback.models.CuadroDiabetico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author David Silva
 */
@Repository
public interface CuadroDiabeticoRepository extends JpaRepository<CuadroDiabetico, Long>{
    
    

    
}
