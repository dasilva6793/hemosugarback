package com.seminario.hemosugarback.repository;

import com.seminario.hemosugarback.models.ERole;
import com.seminario.hemosugarback.models.Role;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author David Silva
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(ERole name);
}
