package com.seminario.hemosugarback.controller;

import com.seminario.hemosugarback.models.Eps;
import com.seminario.hemosugarback.repository.EpsRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author David Silva
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/eps")
public class EpsController {

    @Autowired
    EpsRepository epsRepository;

    @GetMapping("/findAll")
    public List<Eps> findAll() {
        List<Eps> epsLst = new ArrayList<>();
        epsLst = epsRepository.findAll();
        return epsLst;
    }

}
