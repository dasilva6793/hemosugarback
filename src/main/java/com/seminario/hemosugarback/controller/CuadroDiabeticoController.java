package com.seminario.hemosugarback.controller;

import com.seminario.hemosugarback.models.CuadroDiabetico;
import com.seminario.hemosugarback.repository.CuadroDiabeticoRepository;
import com.seminario.hemosugarback.repository.RecomendacionesRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author David Silva
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/cuadroDiabetico")
public class CuadroDiabeticoController {

    @Autowired
    CuadroDiabeticoRepository cuadroDiabeticoRepository;

    @Autowired
    RecomendacionesRepository recomendacionesRepository;

    @GetMapping("/examinar")
    public CuadroDiabetico examinar(@RequestParam(value = "glucemia", defaultValue = "0") double glucemia) {        
        CuadroDiabetico cuadroDiabetico = null;
        List<CuadroDiabetico> cuadroDiabeticosLst = cuadroDiabeticoRepository.findAll();

        for (CuadroDiabetico c : cuadroDiabeticosLst) {            
            if (c.getRangoinicial() <= glucemia && glucemia < c.getRangofinal()) {
                cuadroDiabetico = c;
            }
        }

        return cuadroDiabetico;
    }

}
