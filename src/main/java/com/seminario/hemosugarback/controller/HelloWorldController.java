package com.seminario.hemosugarback.controller;

import java.util.List;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author David Silva
 */
@RestController
public class HelloWorldController {

    @RequestMapping("hello")
    public String helloWorld(@RequestParam(value = "name", defaultValue = "World") String name) {
        return "Hello " + name + "!!";
    }

    @GetMapping("getNamesLst")
    public List<String> getNamesLst() {
        List<String> nameLst = new ArrayList<>();
        nameLst.add("David");
        nameLst.add("Luis");
        nameLst.add("Orlando");
        nameLst.add("Diana");
        return nameLst;
    }
}
