/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.hemosugarback.controller;

import com.seminario.hemosugarback.models.CuadroDiabetico;
import com.seminario.hemosugarback.models.Recomendaciones;
import com.seminario.hemosugarback.repository.RecomendacionesRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author David Silva
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/recomendaciones")
public class RecomendacionesController {
    
    @Autowired
    RecomendacionesRepository recomendacionesRepository;
    
    @GetMapping("/recomendacioenesByCuadro")
    public List<Recomendaciones> recomendacioenesByCuadro(@RequestParam(value = "cuadro", defaultValue = "0") Long cuadro) {
        
        List<Recomendaciones> recomendacionesLst = new ArrayList<>();
        CuadroDiabetico d = new CuadroDiabetico();
        d.setId(cuadro);
        recomendacionesLst = recomendacionesRepository.getRecomendacionesByCudre(d);        
        return recomendacionesLst; 
        
    }
    
}
